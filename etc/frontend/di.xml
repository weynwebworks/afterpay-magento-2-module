<?xml version="1.0"?>
<!--
/**
 * Copyright (c) 2020  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2020 arvato Finance B.V.
 */
-->
<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:framework:ObjectManager/etc/config.xsd">
    <type name="Magento\Checkout\Model\CompositeConfigProvider">
        <arguments>
            <argument name="configProviders" xsi:type="array">
                <item name="afterpay_checkout_message_timeout" xsi:type="object">Afterpay\Payment\Model\Config\Provider\MessageTimeout</item>
                <item name="afterpay_nl_digital_invoice" xsi:type="object">Afterpay\Payment\Model\Config\Provider\DigitalInvoiceNL\ConfigProvider</item>
                <item name="afterpay_nl_direct_debit" xsi:type="object">Afterpay\Payment\Model\Config\Provider\DigitalInvoiceNLDebit\ConfigProvider</item>
                <item name="afterpay_nl_digital_invoice_extra" xsi:type="object">Afterpay\Payment\Model\Config\Provider\DigitalInvoiceNLExtra\ConfigProvider</item>
                <item name="afterpay_nl_business_2_business" xsi:type="object">Afterpay\Payment\Model\Config\Provider\B2BNL\ConfigProvider</item>
                <item name="afterpay_nl_rest_invoice" xsi:type="object">Afterpay\Payment\Model\Config\Provider\InvoiceRestNL\ConfigProvider</item>
                <item name="afterpay_nl_rest_direct_debit" xsi:type="object">Afterpay\Payment\Model\Config\Provider\DirectDebitRestNL\ConfigProvider</item>
                <item name="afterpay_nl_rest_b2b" xsi:type="object">Afterpay\Payment\Model\Config\Provider\B2BRestNL\ConfigProvider</item>
                <item name="afterpay_de_digital_invoice_" xsi:type="object">Afterpay\Payment\Model\Config\Provider\DigitalInvoiceDE\ConfigProvider</item>
                <item name="afterpay_de_direct_debit" xsi:type="object">Afterpay\Payment\Model\Config\Provider\DirectDebitDE\ConfigProvider</item>
                <item name="afterpay_de_digital_invoice_extra" xsi:type="object">Afterpay\Payment\Model\Config\Provider\DigitalInvoiceDEExtra\ConfigProvider</item>
                <item name="afterpay_de_b2b" xsi:type="object">Afterpay\Payment\Model\Config\Provider\B2BDE\ConfigProvider</item>
                <item name="afterpay_de_installment" xsi:type="object">Afterpay\Payment\Model\Config\Provider\InstallmentDE\ConfigProvider</item>
                <item name="afterpay_be_digital_invoice" xsi:type="object">Afterpay\Payment\Model\Config\Provider\DigitalInvoiceBE\ConfigProvider</item>
                <item name="afterpay_be_digital_invoice_extra" xsi:type="object">Afterpay\Payment\Model\Config\Provider\DigitalInvoiceBEExtra\ConfigProvider</item>
                <item name="afterpay_be_rest_invoice" xsi:type="object">Afterpay\Payment\Model\Config\Provider\InvoiceRestBE\ConfigProvider</item>
                <item name="afterpay_be_rest_direct_debit" xsi:type="object">Afterpay\Payment\Model\Config\Provider\DirectDebitRestBE\ConfigProvider</item>
                <item name="afterpay_be_rest_b2b" xsi:type="object">Afterpay\Payment\Model\Config\Provider\B2BRestBE\ConfigProvider</item>
                <item name="afterpay_at_open_invoice" xsi:type="object">Afterpay\Payment\Model\Config\Provider\OpenInvoiceAT\ConfigProvider</item>
                <item name="afterpay_at_installment" xsi:type="object">Afterpay\Payment\Model\Config\Provider\InstallmentAT\ConfigProvider</item>
                <item name="afterpay_at_direct_debit" xsi:type="object">Afterpay\Payment\Model\Config\Provider\DirectDebitAT\ConfigProvider</item>
                <item name="afterpay_ch_open_invoice" xsi:type="object">Afterpay\Payment\Model\Config\Provider\OpenInvoiceCH\ConfigProvider</item>
                <item name="afterpay_se_open_invoice" xsi:type="object">Afterpay\Payment\Model\Config\Provider\OpenInvoiceSE\ConfigProvider</item>
                <item name="afterpay_se_installment" xsi:type="object">Afterpay\Payment\Model\Config\Provider\InstallmentSE\ConfigProvider</item>
                <item name="afterpay_fi_open_invoice" xsi:type="object">Afterpay\Payment\Model\Config\Provider\OpenInvoiceFI\ConfigProvider</item>
                <item name="afterpay_fi_installment" xsi:type="object">Afterpay\Payment\Model\Config\Provider\InstallmentFI\ConfigProvider</item>
                <item name="afterpay_no_open_invoice" xsi:type="object">Afterpay\Payment\Model\Config\Provider\OpenInvoiceNO\ConfigProvider</item>
                <item name="afterpay_no_installment" xsi:type="object">Afterpay\Payment\Model\Config\Provider\InstallmentNO\ConfigProvider</item>
                <item name="afterpay_no_flex" xsi:type="object">Afterpay\Payment\Model\Config\Provider\FlexNO\ConfigProvider</item>
                <item name="afterpay_dk_digital_invoice" xsi:type="object">Afterpay\Payment\Model\Config\Provider\DigitalInvoiceDK\ConfigProvider</item>
            </argument>
        </arguments>
    </type>
    <!-- NL -->
    <virtualType name="Afterpay\Payment\Model\Config\Provider\DigitalInvoiceNL\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NL_DI</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\DigitalInvoiceNL\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\DigitalInvoiceNLDebit\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NL_DD</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\DigitalInvoiceNLDebit\Config
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\DigitalInvoiceNLExtra\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NL_DI_EXTRA</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\DigitalInvoiceNLExtra\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\B2BNL\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NL_B2B</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\B2BNL\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\InvoiceRestNL\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NL_REST_DI</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\InvoiceRestNL\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\DirectDebitRestNL\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NL_REST_DD</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\DirectDebitRestNL\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\B2BRestNL\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NL_REST_B2B</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\B2BRestNL\Config</argument>
        </arguments>
    </virtualType>
    <!-- DE -->
    <virtualType name="Afterpay\Payment\Model\Config\Provider\DigitalInvoiceDE\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_DE_DI</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\DigitalInvoiceDE\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\DigitalInvoiceDEExtra\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_DE_DI_EXTRA</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\DigitalInvoiceDEExtra\Config
            </argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\DirectDebitDE\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_DE_DD</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\DirectDebitDE\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\InstallmentDE\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_DE_IN</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\InstallmentDE\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\B2BDE\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_DE_B2B</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\B2BDE\Config</argument>
        </arguments>
    </virtualType>
    <!-- BE -->
    <virtualType name="Afterpay\Payment\Model\Config\Provider\DigitalInvoiceBE\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_BE_DI</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\DigitalInvoiceBE\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\DigitalInvoiceBEExtra\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_BE_DI_EXTRA</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\DigitalInvoiceBEExtra\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\InvoiceRestBE\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_BE_REST_DI</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\InvoiceRestBE\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\DirectDebitRestBE\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_BE_REST_DD</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\DirectDebitRestBE\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\B2BRestBE\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_BE_REST_B2B</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\B2BRestBE\Config</argument>
        </arguments>
    </virtualType>
    <!-- AT -->
    <virtualType name="Afterpay\Payment\Model\Config\Provider\OpenInvoiceAT\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_AT_OI</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\OpenInvoiceAT\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\DirectDebitAT\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_AT_DD</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\DirectDebitAT\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\InstallmentAT\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_AT_IN</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\InstallmentAT\Config</argument>
        </arguments>
    </virtualType>
    <!-- CH -->
    <virtualType name="Afterpay\Payment\Model\Config\Provider\OpenInvoiceCH\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_CH_OI</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\OpenInvoiceCH\Config</argument>
        </arguments>
    </virtualType>
    <!-- SE -->
    <virtualType name="Afterpay\Payment\Model\Config\Provider\OpenInvoiceSE\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_SE_OI</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\OpenInvoiceSE\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\InstallmentSE\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_SE_IN</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\InstallmentSE\Config</argument>
        </arguments>
    </virtualType>
    <!-- FI -->
    <virtualType name="Afterpay\Payment\Model\Config\Provider\OpenInvoiceFI\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_FI_OI</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\OpenInvoiceFI\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\InstallmentFI\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_FI_IN</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\InstallmentFI\Config</argument>
        </arguments>
    </virtualType>
    <!-- NO -->
    <virtualType name="Afterpay\Payment\Model\Config\Provider\OpenInvoiceNO\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NO_OI</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\OpenInvoiceNO\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\InstallmentNO\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NO_IN</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\InstallmentNO\Config</argument>
        </arguments>
    </virtualType>
    <virtualType name="Afterpay\Payment\Model\Config\Provider\FlexNO\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_NO_FX</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\FlexNO\Config</argument>
        </arguments>
    </virtualType>
    <!-- DK -->
    <virtualType name="Afterpay\Payment\Model\Config\Provider\DigitalInvoiceDK\ConfigProvider"
                 type="Afterpay\Payment\Model\Config\Provider\Base\ConfigProvider">
        <arguments>
            <argument name="code" xsi:type="const">Afterpay\Payment\Helper\Service\Data::AFTERPAY_DK_DI</argument>
            <argument name="config" xsi:type="object">Afterpay\Payment\Gateway\Config\DigitalInvoiceDK\Config</argument>
        </arguments>
    </virtualType>
</config>
