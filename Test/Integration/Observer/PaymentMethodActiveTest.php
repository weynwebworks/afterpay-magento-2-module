<?php

namespace Afterpay\Payment\Test\Integration\Observer;

use Afterpay\Payment\Model\Config\Visitor;
use Magento\Quote\Model\Quote;
use Magento\TestFramework\ObjectManager;
use PHPUnit\Framework\TestCase;

class PaymentMethodActiveTest extends TestCase
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $sharedVisitorMock;

    /**
     * Relies on order, needs to be first
     *
     * @magentoConfigFixture default_store payment/afterpay_nl_digital_invoice/active 1
     * @magentoConfigFixture default_store payment/afterpay_nl_digital_invoice/restrict 1
     */
    public function testDevIpWhitelistWhenEnabled()
    {
        $method = $this->getMethodInstance();

        $mockQuote = $this->getMockBuilder(Quote::class)->disableOriginalConstructor()
            ->getMock();

        $this->sharedVisitorMock->method('allowedByIp')->willReturnOnConsecutiveCalls(false, true);
        $allowed = $method->isAvailable($mockQuote);
        $this->assertEquals(false, $allowed, 'Should NOT show when IP restricted');

        $allowed = $method->isAvailable($mockQuote);
        $this->assertEquals(true, $allowed, 'Should show when IP allowed');
    }

    /**
     * @magentoConfigFixture default_store payment/afterpay_nl_digital_invoice/active 1
     * @magentoConfigFixture default_store payment/afterpay_nl_digital_invoice/restrict 0
     */
    public function testDevIpAllowAnyWhenConfigDisabled()
    {
        $method = $this->getMethodInstance();

        $allowed = $method->isAvailable(
            $this->getMockBuilder(Quote::class)->disableOriginalConstructor()->getMock()
        );

        $this->assertEquals(true, $allowed, 'Should show when disabled config');
    }

    /**
     * @magentoConfigFixture default_store payment/afterpay_nl_digital_invoice/active true
     * @magentoConfigFixture default_store payment/afterpay_nl_digital_invoice/allowspecificgroup 1
     * @magentoConfigFixture default_store payment/afterpay_nl_digital_invoice/specificgroup 5
     */
    public function testNotAllowedForWrongGroup()
    {
        $mockQuote = $this->objectManager->get(Quote::class);
        $mockQuote->setCustomerGroupId('6');

        $methodClass = 'DigitalInvoiceNLFacade';
        $method = $this->objectManager->get($methodClass);
        $allowed = $method->isAvailable($mockQuote);

        $this->assertEquals(false, $allowed, sprintf('%s should not be allowed', $methodClass));
    }

    /**
     * @magentoConfigFixture default_store payment/afterpay_nl_digital_invoice/active true
     * @magentoConfigFixture default_store payment/afterpay_nl_digital_invoice/allowspecificgroup 1
     * @magentoConfigFixture default_store payment/afterpay_nl_digital_invoice/specificgroup 5,7
     */
    public function testAllowedForConfiguredGroup()
    {
        $mockQuote = $this->objectManager->get(Quote::class);
        $mockQuote->setCustomerGroupId('5');

        $methodClass = 'DigitalInvoiceNLFacade';
        $method = $this->objectManager->get($methodClass);
        $allowed = $method->isAvailable($mockQuote);

        $this->assertEquals(true, $allowed, sprintf('%s should be allowed for configured groups', $methodClass));
    }

    /**
     * @magentoConfigFixture default_store payment/afterpay_nl_digital_invoice/active true
     * @magentoConfigFixture default_store payment/afterpay_nl_digital_invoice/excludeships flatrate_flatrate
     */
    public function testRestrictedForShippingMethod()
    {
        $mockAddress = $this->getMockBuilder(Quote\Address::class)->disableOriginalConstructor()->getMock();
        $mockQuote = $this->getMockBuilder(Quote::class)->disableOriginalConstructor()->getMock();
        $mockQuote->method('getShippingAddress')->willReturn($mockAddress);
        $mockAddress->method('getShippingMethod')->willReturn('flatrate_flatrate');

        $method = $this->getMethodInstance();
        $allowed = $method->isAvailable($mockQuote);

        $this->assertEquals(false, $allowed, 'Should not be allowed when restricted shipping method selected');
    }

    protected function setUp()
    {
        parent::setUp();
        $this->objectManager = ObjectManager::getInstance();
        $this->sharedVisitorMock = $this->getMockBuilder(Visitor::class)
            ->disableOriginalConstructor()->getMock();

        $this->objectManager->configure([\Magento\Developer\Helper\Data::class => ['shared' => true]]);
        $this->objectManager->addSharedInstance($this->sharedVisitorMock, Visitor::class);
    }

    protected function tearDown()
    {
        parent::tearDown();
        $this->objectManager->removeSharedInstance(Visitor::class);
        $this->objectManager = null;
    }

    /**
     * @return \Magento\Payment\Model\MethodInterface
     */
    private function getMethodInstance()
    {
        $methodClass = 'DigitalInvoiceNLFacade';
        return $this->objectManager->get($methodClass);
    }
}
