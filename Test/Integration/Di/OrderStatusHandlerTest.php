<?php

namespace Afterpay\Payment\Test\Integration\Di;

use Afterpay\Payment\Gateway\Command\CaptureStrategyCommand;
use Afterpay\Payment\Test\Integration\BaseIntegrationCase;

class OrderStatusHandlerTest extends BaseIntegrationCase
{
    /**
     * @magentoConfigFixture default_store payment/afterpay_capture/order_status_accepted teststatus
     */
    public function testReturnsConfiguredStatus()
    {
        $registry = $this->objectManager->get(\Magento\Framework\Registry::class);
        $registry->register(CaptureStrategyCommand::KEY_AFTERPAY_CAPTURE_IN_PROGRESS, 1);

        $method = $this->getMethodInstance();
        $this->assertEquals('teststatus', $method->getConfigData('order_status'));
        $registry->unregister(CaptureStrategyCommand::KEY_AFTERPAY_CAPTURE_IN_PROGRESS);
    }

    /**
     * @magentoConfigFixture default_store payment/afterpay_capture/order_status_accepted teststatus
     */
    public function testReturnsDefaultWhenAuthorizing()
    {
        $method = $this->getMethodInstance();
        $this->assertEquals('processing', $method->getConfigData('order_status'));
    }
}
