<?php
/**
 * Copyright (c) 2020  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2020 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Gateway\Request;

use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Checkout\Model\Session;
use Magento\Framework\Exception\LocalizedException;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Framework\Module\Manager;
use Afterpay\Payment\Helper\Service\Data;

class UPSAPAddressBuilder implements BuilderInterface
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * @var Data
     */

    protected $helper;
    /**
     * @var SubjectReader
     */

    protected $subjectReader;

    /**
     * @var Manager
     */
    private $manager;

    /**
     * @param Manager $manager
     * @param Session $session
     * @param Data $helper
     * @param SubjectReader $subjectReader
     */
    public function __construct(
        Manager $manager,
        Session $session,
        Data $helper,
        SubjectReader $subjectReader
    ) {
        $this->manager = $manager;
        $this->session = $session;
        $this->helper = $helper;
        $this->subjectReader = $subjectReader;
    }

    /**
     * @inheritdoc
     *
     * @throws LocalizedException
     */
    public function build(array $buildSubject): array
    {
        $paymentDO = $this->subjectReader::readPayment($buildSubject);
        $order = $paymentDO->getPayment()->getOrder();
        $data = [];
        if ($this->manager->isEnabled('Infomodus_Upsap')) {
            if ($this->helper->getStoreConfig('carriers/upsap/active', $order->getStoreId())) {
                if ($this->session->getUpsapAddLine1()) {
                    $address = implode(
                        ' ',
                        [
                            $this->session->getUpsapAddLine1(),
                            $this->session->getUpsapAddLine2(),
                            $this->session->getUpsapAddLine3()
                        ]
                    );
                    $address = $this->helper->getSplitStreet($address);
                    $data = [
                        'shiptoaddress' => [
                            'isocountrycode' => $this->session->getUpsapCountry(),
                            'city' => $this->session->getUpsapCity(),
                            'postalcode' => $this->session->getUpsapPostal(),
                            'streetname' => $address['streetname'],
                            'housenumber' => $address['housenumber'],
                            'housenumberaddition' => $address['houseNumberAddition'],
                            'referenceperson' =>
                                [
                                    'initials' => 'A',
                                    'firstname' => 'A',
                                    'lastname' => 'UPS ' . $this->session->getUpsapName(),
                                ]
                        ]
                    ];
                }
            }
        }

        return $data;
    }
}
