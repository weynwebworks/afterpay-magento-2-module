<?php
/**
 * Copyright (c) 2020  arvato Finance B.V.
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @category    AfterPay
 * @package     Afterpay_Payment
 * @copyright   Copyright (c) 2020 arvato Finance B.V.
 */

declare(strict_types=1);

namespace Afterpay\Payment\Gateway\Http\Client;

use Afterpay\AfterpayFactory;
use Afterpay\Payment\Helper\Debug\Data as DebugHelper;
use Afterpay\Payment\Helper\Service\Data as Helper;
use GuzzleHttp\Exception\GuzzleException;
use Magento\Payment\Model\Method\Logger;
use Psr\Log\LoggerInterface;

class CaptureSale extends AbstractTransaction
{
    const ORDER_MANAGEMENT_CODE = 'OM';

    /**
     * Debug
     *
     * @var DebugHelper
     */
    protected $debugHelper;

    /**
     * @var Helper
     */
    protected $helper;

    /**
     * TransactionSale constructor.
     *
     * @param LoggerInterface $logger
     * @param Logger $customLogger
     * @param AfterpayFactory $afterpayFactory
     * @param DebugHelper $debugHelper
     * @param Helper $helper
     */
    public function __construct(
        LoggerInterface $logger,
        Logger $customLogger,
        AfterpayFactory $afterpayFactory,
        DebugHelper $debugHelper,
        Helper $helper
    ) {
        parent::__construct($logger, $customLogger, $afterpayFactory);
        $this->debugHelper = $debugHelper;
        $this->helper = $helper;
    }

    /**
     * @inheritdoc
     * @throws GuzzleException
     */
    protected function execute(array $data, array $clientConfig)
    {
        $captureType = 'capture_full';
        if ($this->isRestOrder($clientConfig)) {
            $this->afterpay->setRest();
        } else {
            unset($data['totalamount']);
        }
        /**
         * This isn't the best way to handle this case because SOAP and REST clients are used and
         * both of them expect different discount (i.e. positive or negative amount) and data can't be accessed in
         * data builder regarding if REST or SOAP is used..
         */
        if (array_key_exists('orderlines', $data)
            && array_key_exists('tempDiscount', $data['orderlines'])) {
            /** @var array $discountLine */
            $discountLine = $data['orderlines']['tempDiscount'];
            unset($data['orderlines']['tempDiscount']);
            if ($this->isRest()) {
                $discountLine[5] *= -1; //VAT amount
            }
            $data['orderlines'][] = $discountLine;
        }
        if (array_key_exists('orderlines', $data) && $data['partialinvoice']) {
            $captureType = 'capture_partial';
            $this->addOrderLines($data['orderlines']);
        }
        if (array_key_exists('orderlines', $data) && $data['specialCaseInvoice']) {
            $this->addOrderLines($data['orderlines']);
        }

        $this->afterpay->set_ordermanagement($captureType);
        $this->afterpay->set_order($data, self::ORDER_MANAGEMENT_CODE);
        $this->afterpay->do_request(
            $clientConfig,
            $clientConfig['modus'],
            $this->helper->getCurrentLocaleNormalized()
        );

        return $this->afterpay->order_result->return;
    }

    /**
     * @return bool
     */
    private function isRest(): bool
    {
        return $this->afterpay->useRest;
    }

    /**
     * @param $orderlines
     */
    private function addOrderLines($orderlines)
    {
        foreach ($orderlines as $line) {
            $this->afterpay->create_order_line(...array_values($line));
        }
    }
}
